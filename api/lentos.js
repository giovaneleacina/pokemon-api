/*
    Lista apenas os pokemons com menos de 40 pontos de velocidade (speed)
    A lista deve estar ordenada do mais lento para o mais rápido
*/

var data = require('../data')

module.exports = function(req, res){

    var result = []

    //Implementação
    result = data.filter(function(pokemon){
        return pokemon.speed < 40
    })
    
    //Retorno
    res.json(result)
}